#!/bin/bash

# einstellen der favoriten
# xmessage -center Hallo

# anzeigen der systemparameter

zenity --info --text "$(lshw -C memory)\n------------------------------------\nAnzahl Kerne: $(nproc)\n------------------------------------\n$(lshw -C cpu)" --width 1024

snap ack chromium_1143.assert && snap install chromium_1143.snap
gnome-terminal -- bash -c "sudo snap ack /home/schule/chromium_1143.assert; sudo snap install /home/schule/chromium_1143.snap"

dconf write /org/gnome/shell/favorite-apps "['chromium_chromium.desktop', 'thunderbird.desktop', 'org.gnome.Nautilus.desktop', 'libreoffice-writer.desktop', 'libreoffice-calc.desktop', 'libreoffice-impress.desktop', 'org.gnome.Software.desktop']"

# zeige nach reboot bei erster verbindung die wilkommen-seite
systemctl enable --user heyalter.service

# richte das hintergrundbild ein
gsettings set org.gnome.desktop.background picture-uri 'file:///home/schule/Bilder/los_gehts.png'

cheese

chromium-browser

touch /home/schule/setup_done