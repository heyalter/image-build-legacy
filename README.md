# legacy

Mehr oder weniger relevante files im bisherigen imagebau.

Die `cubic.conf` beinhaltet die allgemeine Konfiguration von cubic. Die `ubuntu.seed`, `cli.seed`, sowie `ltsp.seed` dürften preseed Dateien fürs image sein, die in der alten Version von cubic noch in cubic selbst editiert werden konnten.

`setup.sh` und `cleanup.sh` lagen im fertig installierten image auf dem Desktop und wurden nacheinander ausgeführt.

Das `setup.sh`-Skirpt zeigt zunächst ein paar Systemdaten an, CPU, RAM, etc. und installiert dann Chromium, dazu erwartet es die Dateien `chromium_1143.assert` und `chromium_1143.snap` in `/home/schule/`. Danach passt es die Favoriten an (Icons im Dock links) und enabled den `heyalter.service`.
Das Hintergrundbild wird gesetzt und dann noch das Programm `cheese` gestartet um die Webcam zu testen.